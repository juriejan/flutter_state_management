import 'package:flutter/material.dart';

import 'package:flutter_state_management/screens/home.dart';

void main() => runApp(Application());

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter State Management',
      home: HomeScreen(),
    );
  }
}
