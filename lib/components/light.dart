import 'package:flutter/material.dart';

class Light extends StatelessWidget {
  final bool isOn;

  Light({
    key,
    this.isOn,
  })  : assert(isOn != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50.0,
      height: 50.0,
      decoration: BoxDecoration(
        color: isOn ? Colors.red : Colors.black,
      ),
    );
  }
}
