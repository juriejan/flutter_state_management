import 'package:flutter/material.dart';

class Toggle extends StatefulWidget {
  final bool isOn;
  final Function(bool value) onChanged;

  Toggle({
    key,
    this.isOn,
    this.onChanged,
  })  : assert(isOn != null),
        super(key: key);

  @override
  _ToggleState createState() => _ToggleState();
}

class _ToggleState extends State<Toggle> {
  bool isOn;

  @override
  void initState() {
    isOn = widget.isOn;
    super.initState();
  }

  @override
  void didUpdateWidget(Toggle oldWidget) {
    isOn = widget.isOn;
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isOn = !isOn;
        });
        if (widget.onChanged != null) {
          widget.onChanged(isOn);
        }
      },
      child: Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          border: Border.all(),
        ),
        child: Text(isOn ? 'ON' : 'OFF'),
      ),
    );
  }
}
