import 'package:flutter/material.dart';

import 'package:flutter_state_management/components/light.dart';
import 'package:flutter_state_management/components/toggle.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isLightOn;

  @override
  void initState() {
    isLightOn = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Light(
              isOn: isLightOn,
            ),
            SizedBox(
              height: 20.0,
            ),
            Toggle(
              isOn: isLightOn,
              onChanged: (bool value) {
                setState(() {
                  isLightOn = value;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
